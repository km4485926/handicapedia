import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { globalStyles } from '../styles/global';

export default function vignetteHandicap(props) {

const [color, setColor] = useState('#aec');

    const getColor = async () => {
        try {
         const response = await fetch('https://www.thecolorapi.com/scheme?hex=0C72B7&count=5&mode=monochrome');
         let json = await response.json();
         let myColors = json.colors;
         var nb = Math.floor(Math.random() * myColors.length);
         console.log(myColors[nb].hex.value);
         setColor(myColors[nb].hex.value);
       } catch (error) {
         setColor("#aec");
       }
     }

getColor();

    return (
        <View style ={[styles.vignetteHandicap, {backgroundColor:color}]}>
            <View style={styles.contenuVignette}>
            {props.children}
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    vignetteHandicap : {
        borderRadius: 6,
        borderWidth:1.5,
        borderColor:'black',
        elevation: 3,
        backgroundColor:'#aec',
        shadowOffset: {width: 1, height: 1},
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        marginHorizontal: 4,
        marginVertical: 6,
    },
    contenuVignette : {
        marginHorizontal: 18,
        marginVertical: 10,
        color:'white'
    }
})