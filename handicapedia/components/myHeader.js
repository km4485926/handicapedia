import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';

export default function myHeader() {
    return (
        <View style = {styles.header}>
            <View style={styles.headerTitle}>
                <Text style={styles.headerText}>Handicapedia</Text>
                <Image source={require('../assets/logoHandicapedia.png')} style={styles.headerImage}/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#333',
        letterSpacing:1,
    },
    headerImage: {
        width:26,
        height:26,
        marginHorizontal:10,
        alignSelf:'flex-end'
    },
    headerTitle: {
        flexDirection:'row',
        justifyContent:'space-evenly'
    }
})