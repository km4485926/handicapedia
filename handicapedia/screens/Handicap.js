import React from 'react';
import { View, Text } from 'react-native';
import { globalStyles } from '../styles/global';
import Vignette from  '../components/vignetteHandicap';

export default function Handicap({route, navigation}) {

  return (
    <View style={globalStyles.container}>
      <Vignette>
      <Text style={globalStyles.titleText}>{route.params.nom}</Text>
      <Text style={globalStyles.titleText}>{route.params.categorie}</Text>
      <Text style={globalStyles.titleText}>{route.params.description}</Text>
      <Text style={globalStyles.titleText}>{route.params.lien}</Text>
      </Vignette>
    </View>
  );
}