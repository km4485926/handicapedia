import React from 'react';
import { View, Text, Button } from 'react-native';
import { globalStyles } from '../styles/global';

export default function ReviewDetails({route, navigation}) {

  return (
    <View style={globalStyles.container}>
      <Button
        title="Consulter"
        onPress={() => navigation.navigate("Main")}
      />
      <Button
        title="A propos"
        onPress={() => navigation.navigate("About")}
      />
    </View>
  );
}
