import React, {Component} from 'react';
import { StyleSheet, View, Text, Linking, Modal, Button } from 'react-native';
import { globalStyles } from '../styles/global';
import renderIf from '../renderIf';


export default function AboutFunction({route, navigation}){
class About extends Component {
  constructor(){
    super();
    this.state ={
      status:false
    }
  }

  toggleStatus(){
    this.setState({
      status:!this.state.status
    });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#0C72B7", alignItems:'center' }}>
        <View style={{backgroundColor:'white', padding:10, borderRadius:5, borderColor:'black', marginBottom:20, marginTop:10}}>
      <Text style={styles.TitreAbout}
      onPress={() => Linking.openURL('https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstley') && this.toggleStatus()}>
  A propos de Handicapedia
    </Text>
        </View>
    {renderIf(this.state.status)(
        <View>
          <View style={{backgroundColor:'white', padding:5, borderRadius:5, marginTop:10, borderColor:'black'}}>
          <Text style={styles.TexteAbout}>
            Ceci est une application créée par Karim MOSBAH et Maël TROUILLEZ-DAHMANE (INSA Hdf 3A).
          </Text>
          </View>
          <View style={{backgroundColor:'white', padding:5, borderRadius:5, marginTop:10, borderColor:'black'}}>
          <Text style={styles.TexteAbout}>
            Le projet n'est pas de fournir une quelconque fonctionnalité aux personnes atteintes de handicaps elles-mêmes. 
            Il s'agit de participer à éduquer sur la diversité du handicap pour faire du monde un endroit plus facile à vivre pour les personnes atteintes.
          </Text>
          </View>
          <View style={{backgroundColor:'white', padding:5, borderRadius:5, marginTop:10, borderColor:'black'}}>
          <Text style={styles.LinkedIn}
          onPress={() => Linking.openURL('https://www.linkedin.com/in/karim-mosbah-49994a211/')}>
            Karim : LinkedIn
          </Text>
          </View>
          <View style={{backgroundColor:'white', padding:5, borderRadius:5, marginTop:10, borderColor:'black'}}>
          <Text style={styles.LinkedIn}
          onPress={() => Linking.openURL('https://www.linkedin.com/in/mael-trouillez-dahmane/')}>
            Maël : LinkedIn
          </Text>
          </View>

        </View>
        )}
      </View>
    );
  }
}
return <About/>
}

const styles = StyleSheet.create({
  TitreAbout : {
      fontSize:20,
      fontFamily:'Nunito-Regular',
      fontWeight:'bold',
      color: 'blue',
      borderRadius: 6,
      elevation: 5,
      backgroundColor:'#fff',
      shadowOffset: {width: 1, height: 1},
      shadowColor: '#333',
      shadowOpacity: 0.3,
      shadowRadius: 2,
      marginHorizontal: 3,
      marginVertical: 3,
  },
  TexteAbout : {
      marginHorizontal: 2,
      marginVertical: 2,
      justifyContent:'space-around'
  },
  LinkedIn : {
    marginHorizontal: 2,
    marginVertical: 2,
    justifyContent:'flex-start',
    color:'blue'
}
})