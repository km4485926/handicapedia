import React, {useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Button } from 'react-native';
import { globalStyles } from '../styles/global';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useLinkProps } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';
import Vignette from  '../components/vignetteHandicap';

function getRandomHandicap() {
  return handicaps[Math.floor(Math.random()*handicaps.length)];
}

function MainList() {
  //On reconnaît 5 catégories de handicaps : handicap moteur, handicap mental, handic psychique, handicap sensoriel et maladie invalidante. (utiliser une liste déroulante).
  //Mettre des placeholders explicatifs dans les boîtes de texte "nom", "description" et "lien".
  const [handicaps, setHandicaps] = useState([
    { nom: 'Paralysie Totale', categorie: 'handicap moteur', description: 'lorem ipsum', lien : 'http//...', key: '1' },
    { nom: 'Déficience Mentale', categorie: 'handicap mental', description: 'lorem ipsum', lien : 'http//...', key: '2' },
    { nom: 'Paralysie partielle', categorie: 'handicap moteur', description: 'lorem ipsum', lien : 'http//..', key: '3' },
    { nom: 'Schizophrénie', categorie: 'handicap psychique', description: 'lorem ipsum', lien : 'http//..', key: '4' },
    { nom: 'Surdité', categorie: 'handicap sensoriel', description: 'lorem ipsum', lien : 'http//..', key: '5' },
    { nom: 'Epilepsie', categorie: 'maladie invalidante', description: 'lorem ipsum', lien : 'http//..', key: '6' }
  ]);

  const navigation = useNavigation();
  return (
    <View style={globalStyles.container}>
      <Button style={{color:'grey'}}
        title="Random"
        onPress={() => navigation.navigate('Handicap', handicaps[Math.floor(Math.random()*handicaps.length)])}
      />
      <FlatList data={handicaps} renderItem={({ item }) => (
        <TouchableOpacity onPress={() => navigation.navigate('Handicap', item)}>
          <Vignette>
            <Text style={globalStyles.titleText}>{ item.nom }</Text>
          </Vignette>
        </TouchableOpacity>
      )} />
    </View>
  );
}



export default function Main({route, navigation}) {

  return (
    <View style={globalStyles.container}>
      <MainList navigation =  {useLinkProps.navigation}></MainList>
      <Button
        title="Create"
        onPress={() => navigation.navigate("Create")}
      />
    </View>
  );
}