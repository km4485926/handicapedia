import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import Home from './screens/Home'
import About from './screens/About'
import Create from './screens/Create'
import Main from './screens/Main'
import Handicap from './screens/Handicap'
import { StyleSheet, Text, View } from 'react-native';
import * as Font from 'expo-font'
import AppLoading from 'expo-app-loading';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Header from './components/myHeader'

const getFonts = () => Font.loadAsync({
    'Nunito-Regular':require('./assets/fonts/Nunito-Regular.ttf'),
    'Nunito-Bold': require('./assets/fonts/Nunito-Bold.ttf')
  });

const stack = createNativeStackNavigator();


export default function App() {



  const [fontsLoaded, setFontsLoaded] = useState(false);


  if(fontsLoaded){
    return (
      <NavigationContainer>
        <stack.Navigator initialRouteName="Home">
          <stack.Screen name="Home" component={Home} options={{
            headerTitle: ()=> <Header/>,
        }
        }
        />
        <stack.Screen name="About" component={About} options={{
          headerTitle: ()=> <Header/>,
        }
        }/>
        <stack.Screen name="Main" component={Main} options={{
          headerTitle: ()=> <Header/>,
        }
        }/>
        <stack.Screen name="Create" component={Create} options={{title:'Create',
        }
        }/>
        <stack.Screen name="Handicap" component={Handicap} options={{title:'Handicap',
        }
        }/>
        <stack.Screen name="RealAbout" component={Handicap} options={{title:'RealAbout',
        }
        }/>
      </stack.Navigator>
    </NavigationContainer>
    );
  } else {
    return (
    <AppLoading
     startAsync={getFonts}
     onFinish={()=> setFontsLoaded(true)} 
     onError={() => console.log('error')}
    />
    )
  }

}